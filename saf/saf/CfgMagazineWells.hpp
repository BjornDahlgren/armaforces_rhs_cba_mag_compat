class CfgMagazineWells
{
    class CBA_556x45_M21
    {
        RHS_SAF_Mags[] =
        {
            //30 round Zastava M21 mags
            "rhsgref_30rnd_556x45_m21",
            "rhsgref_30rnd_556x45_m21_t"
        };
    };
    class CBA_556x45_G36
    {
        RHS_SAF_Mags[] =
        {
            //30 round G36 mags
            "rhssaf_30rnd_556x45_EPR_G36",
            "rhssaf_30rnd_556x45_SOST_G36",
            "rhssaf_30rnd_556x45_SPR_G36",
            "rhssaf_30rnd_556x45_Tracers_G36",
            "rhssaf_30rnd_556x45_MDIM_G36",
            "rhssaf_30rnd_556x45_TDIM_G36",
            //100 round G36 mag
            "rhssaf_100rnd_556x45_EPR_G36"
        };
    };
    class CBA_762x39_AK
    {
        RHS_SAF_Mags[] =
        {
            //30 round Zastava M-70 mags
            "rhssaf_30Rnd_762x39mm_M67",
            "rhssaf_30Rnd_762x39mm_M78_tracer",
            "rhssaf_30Rnd_762x39_M82_api"
        };
    };
    class CBA_762x54R_LINKS
    {
        RHS_SAF_Belts[] =
        {
            //250 round M84 box
            "rhssaf_250Rnd_762x54R"
        };
    };
    class CBA_32ACP_Vz61
    {
        RHS_SAF_Mags[] =
        {
            //20 round M84A Scorpion mag
            "rhsgref_20rnd_765x17_vz61",
            //10 round M84A Scorpion mag
            "rhsgref_10rnd_765x17_vz61"
        };
    };
    class CBA_792x57_M76
    {
        RHS_SAF_Mags[] =
        {
            //10 round M-76 mags
            "rhssaf_10Rnd_792x57_m76_tracer",
            "rhsgref_10Rnd_792x57_m76"
        };
    };
};
