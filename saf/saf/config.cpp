class CfgPatches
{
    class armaforces_rhs_cba_mag_compat_saf
    {
        name = "ArmaForces - RHS CBA Mag Compat - SAF";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"rhssaf_c_weapons", "cba_main"};
        author = "3Mydlo3, veteran29";
    };
};

#include "CfgMagazineWells.hpp"
#include "CfgWeapons.hpp"
