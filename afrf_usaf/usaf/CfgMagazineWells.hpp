class CfgMagazineWells
{
    class CBA_12g_8rnds 
    {
        RHS_USAF_Mags[] =
        {
            //8 round 12 gauge shells (M590A1)
            "rhsusf_8Rnd_00Buck",
            "rhsusf_8Rnd_FRAG",
            "rhsusf_8Rnd_HE",
            "rhsusf_8Rnd_Slug"
        };
    };
    class CBA_300WM_AICS 
    {
        RHS_USAF_Mags[] = 
        {
            //5 rounds (M2010 ESR)
            "rhsusf_5Rnd_300winmag_xm2010"
        };
    };
    class CBA_40mm_M203
    {
        RHS_USAF_grenades[] =
        {
            //M203 grenades
            //Explosive
            "rhs_mag_M441_HE",
            "rhs_mag_M433_HEDP",
            "rhs_mag_M397_HET",
            //Stun
            "rhs_mag_M4009",
            //Buckshot
            "rhs_mag_m576",
            //Flare
            "rhs_mag_M585_white",
            "rhs_mag_M661_green",
            "rhs_mag_M662_red",
            //Smoke
            "rhs_mag_M713_red",
            "rhs_mag_M714_white",
            "rhs_mag_M715_green",
            "rhs_mag_M716_yellow"
        };
    };
    class CBA_40mm_M203_6rnds
    {
        RHS_USAF_grenades[] = 
        {
            //6 loose M203 type grenades (M32 MGL)
            //Explosive
            "rhsusf_mag_6Rnd_M397_HET",
            "rhsusf_mag_6Rnd_M433_HEDP",
            "rhsusf_mag_6Rnd_M441_HE",
            //Stun
            "rhsusf_mag_6Rnd_m4009",
            //Buckshot
            "rhsusf_mag_6Rnd_M576_Buckshot",
            //Flare
            "rhsusf_mag_6Rnd_M585_white",
            "rhsusf_mag_6Rnd_m661_green",
            "rhsusf_mag_6Rnd_m662_red",
            //Smoke
            "rhsusf_mag_6Rnd_M713_red",
            "rhsusf_mag_6Rnd_M714_white",
            "rhsusf_mag_6Rnd_M715_green",
            "rhsusf_mag_6Rnd_M716_yellow",
            //Practice
            "rhsusf_mag_6Rnd_M781_Practice"
        };
    };
    class CBA_45ACP_1911
    {
        RHS_USAF_Mags[] =
        {
            //7 round M1911 magazine
            "rhsusf_mag_7x45acp_MHP"
        };
    };
    class CBA_46x30_MP7
    {
        RHS_USAF_Mags[] =
        {
            //40 round MP7 mags
            "rhsusf_mag_40Rnd_46x30_AP",
            "rhsusf_mag_40Rnd_46x30_FMJ",
            "rhsusf_mag_40Rnd_46x30_JHP"
        };
    };
    class CBA_50BMG_M107
    {
        RHS_USAF_Mags[] =
        {
            //10 round M107 mags
            "rhsusf_mag_10Rnd_STD_50BMG_M33", //standard
            "rhsusf_mag_10Rnd_STD_50BMG_mk211" //HE
        };
    };
    class CBA_556x45_STANAG
    {
        RHS_USAF_mags[] =
        {
            //30 round STANAGs
            "rhs_mag_30Rnd_556x45_M855_Stanag",
            "rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red",
            "rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Green",
            "rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Yellow",
            "rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Orange",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",//Redundant
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
            "rhs_mag_30Rnd_556x45_Mk318_Stanag",
            "rhs_mag_30Rnd_556x45_Mk262_Stanag",
            "rhs_mag_30Rnd_556x45_M200_Stanag",

            //PMAGs
            //Black
            "rhs_mag_30Rnd_556x45_M855A1_PMAG",
            "rhs_mag_30Rnd_556x45_M855A1_PMAG_Tracer_Red",
            "rhs_mag_30Rnd_556x45_M855_PMAG",
            "rhs_mag_30Rnd_556x45_M855_PMAG_Tracer_Red",
            "rhs_mag_30Rnd_556x45_Mk318_PMAG",
            "rhs_mag_30Rnd_556x45_Mk262_PMAG",
            //Tan
            "rhs_mag_30Rnd_556x45_M855A1_PMAG_Tan",
            "rhs_mag_30Rnd_556x45_M855A1_PMAG_Tan_Tracer_Red",
            "rhs_mag_30Rnd_556x45_M855_PMAG_Tan",
            "rhs_mag_30Rnd_556x45_M855_PMAG_Tan_Tracer_Red",
            "rhs_mag_30Rnd_556x45_Mk318_PMAG_Tan",
            "rhs_mag_30Rnd_556x45_Mk262_PMAG_Tan"
        };
    };
    class CBA_556x45_STANAG_2D
    {
        RHS_USAF_mags[] =
        {
            //C-Mags
            "rhs_mag_100Rnd_556x45_M855A1_cmag",
            "rhs_mag_100Rnd_556x45_M855A1_cmag_mixed",
            "rhs_mag_100Rnd_556x45_M855_cmag",
            "rhs_mag_100Rnd_556x45_M855_cmag_mixed",
            "rhs_mag_100Rnd_556x45_Mk318_cmag",
            "rhs_mag_100Rnd_556x45_Mk262_cmag"
        };
    };
    class CBA_556x45_MINIMI
    {
        RHS_USAF_belts[] =
        {
            //200 round plastic box mags
            //M855A1
            "rhsusf_200Rnd_556x45_box",//standard
            "rhsusf_200rnd_556x45_mixed_box",//mixed
            //M855
            "rhsusf_200rnd_556x45_M855_box",//standard
            "rhsusf_200rnd_556x45_M855_mixed_box",//mixed

            //200 round softpacks
            //M855A1
            "rhsusf_200Rnd_556x45_soft_pouch",//standard
            "rhsusf_200Rnd_556x45_mixed_soft_pouch",//mixed
            //M855
            "rhsusf_200Rnd_556x45_M855_soft_pouch",//standard
            "rhsusf_200Rnd_556x45_M855_mixed_soft_pouch",//mixed

            //100 round softpacks
            //M855A1
            "rhsusf_100Rnd_556x45_soft_pouch",//standard
            "rhsusf_100Rnd_556x45_mixed_soft_pouch",//mixed
            //M855
            "rhsusf_100Rnd_556x45_M855_soft_pouch",//standard
            "rhsusf_100Rnd_556x45_M855_mixed_soft_pouch",//mixed
            //M200
            "rhsusf_100Rnd_556x45_M200_soft_pouch" //blanks
        };
    };
    class CBA_762x51_5rnds
    {
        RHS_USAF_Rounds[] =
        {
            //5 loose rounds (M24)
            "rhsusf_5Rnd_762x51_m118_special_Mag",
            "rhsusf_5Rnd_762x51_m62_Mag",
            "rhsusf_5Rnd_762x51_m993_Mag"
        };
    };
    class CBA_762x51_AICS
    {
        RHS_USAF_Mags[] =
        {
            //10 round AICS mags (M40A5)
            "rhsusf_10Rnd_762x51_m118_special_Mag",
            "rhsusf_10Rnd_762x51_m62_Mag",
            "rhsusf_10Rnd_762x51_m993_Mag",
            //5 round AICS mags (M40A5)
            "rhsusf_5Rnd_762x51_AICS_m118_special_Mag",
            "rhsusf_5Rnd_762x51_AICS_m62_Mag",
            "rhsusf_5Rnd_762x51_AICS_m993_Mag",
        };
    };
    class CBA_762x51_AR10
    {
        RHS_USAF_Mags[] =
        {
            //20 round Mk11 magazines
            "rhsusf_20Rnd_762x51_SR25_m118_special_Mag",
            "rhsusf_20Rnd_762x51_SR25_m62_Mag",
            "rhsusf_20Rnd_762x51_SR25_m993_Mag"
        };
    };
    class CBA_762x51_LINKS
    {
        RHS_USAF_belts[] =
        {
            "rhsusf_50Rnd_762x51", // M80
            "rhsusf_50Rnd_762x51_m61_ap",
            "rhsusf_50Rnd_762x51_m62_tracer",
            "rhsusf_50Rnd_762x51_m80a1epr",
            "rhsusf_50Rnd_762x51_m82_blank",

            "rhsusf_100Rnd_762x51", // M80
            "rhsusf_100Rnd_762x51_m61_ap",
            "rhsusf_100Rnd_762x51_m62_tracer",
            "rhsusf_100Rnd_762x51_m80a1epr",
            "rhsusf_100Rnd_762x51_m82_blank"
        };
    };
    class CBA_762x51_M14
    {
        RHS_USAF_Mags[] = 
        {
            //20 round M14 magazines
            "rhsusf_20Rnd_762x51_m118_special_Mag",
            "rhsusf_20Rnd_762x51_m62_Mag",
            "rhsusf_20Rnd_762x51_m993_Mag"
        };
    };
    class CBA_9x19_Glock_Full
    {
        RHS_USAF_Mags[] = 
        {
            //17 round Glock 17 magazines
            "rhsusf_mag_17Rnd_9x19_FMJ",
            "rhsusf_mag_17Rnd_9x19_JHP"
        };
    };
    class CBA_9x19_M9
    {
        RHS_USAF_Mags[] =
        {
            //15 round M9 magazines
            "rhsusf_mag_15Rnd_9x19_JHP",
            "rhsusf_mag_15Rnd_9x19_FMJ"
        };
    };
    class CBA_Carl_Gustaf
    {
        RHS_Rounds[] =
        {
            //MAAWS rounds
            "rhs_mag_maaws_HE",
            "rhs_mag_maaws_HEDP",
            "rhs_mag_maaws_HEAT"
        };
    };
    class CBA_SMAW
    {
        RHS_Rounds[] =
        {
            //SMAW rounds
            "rhs_mag_smaw_HEDP",
            "rhs_mag_smaw_HEAA"
        };
    };
    class CBA_SMAW_Spotting_Rifle
    {
        RHS_Mags[] =
        {
            //SMAW spotting rifle mag
            "rhs_mag_smaw_SR"
        };
    };
};
